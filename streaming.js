secs2time = function (secs){
    var seconds = secs%60;
    var minutes = ((secs-seconds)/60)%60;
    var hours = ((secs-60*minutes-seconds)/3600)%24;

    if (seconds < 10){
        seconds = '0' + parseInt(seconds);
    };
    if (minutes < 10){
        minutes = '0' + minutes;
    };
    if (hours < 10){
        hours = '0' + hours;
    };
    return hours + ":" + minutes + ":" + seconds;
}


var StreamGraph = function(container_id, initial_time, range, update_range, auto_update_time, mode, width_, height_){
    this.range = range;
    this.time = initial_time;
    this.delta_time = 100;
    this.graphics = {};

    d3.select("#" + container_id.replace(" ", "\\ ")).append("div")
                .attr("id", container_id + "lobster");
    this.container_id = container_id + "lobster";
    this.build_graphic(width_, height_);
    this.objects = [];
    if (typeof update_range == "undefined"){
        update_range = true;
    }
    if (typeof auto_update_time == "undefined"){
        auto_update_time = true;
    }
    if (typeof mode == "undefined"){
        mode = 0;
    }
    this.auto_update_time = auto_update_time;
    this.auto_update_range = update_range;
    this.prices_list = [];
    this.mode = mode
    this.__objects_count = 0;
    this.__last_time = initial_time;
    this.__maximum_objects_number = 5;
    this.__refresh_time = 3;
    this.__last_curves = {};
}


StreamGraph.prototype.__reset_count = function(){
    this.__objects_count = 0;
//    this.__single_update(this.__last_time);
    for (var curve in this.graphics){
        var c = this.graphics[curve];
        if (!(curve in this.__last_curves)){
            this.__last_curves[curve] = c.pp;
        }
    }
    this.update_multiple_curves(this.__last_curves, this.__last_time);
    this.__last_curves = {};
}


StreamGraph.prototype.reset = function(initial_time){
    for (var o in this.objects){
        var obj = this.objects[o];
        this.plt.remove(obj);
    }
    this.prices_list = [];
    this.objects = [];
    this.update_margin(initial_time);
    this.__objects_count = 0;
    this.__last_time = initial_time;
    this.__maximum_objects_number = 5;
    this.__refresh_time = 3;
    this.__last_curves = {};
    for (curve in this.graphics){
        this.graphics[curve]['pp'] = undefined;
    }
}


function round_decimals(num, dec){
    return Number(num.toFixed(dec));
}


function round_custom(m){
    var base;
    var new_number;
    var delta;
    var mesh;
    var decimals;
    var suffix;

    var n = Math.abs(m);
    var sign = Math.sign(m);
    if (n <= 1000){
        return round_decimals(n, 3);
    } else if ((1000 <= n) & (n < 1000000)){
        base = 1000.0;
        new_number = n*1.0/base;
        delta = 10.0;
        mesh = 100.0;
        suffix = 'Mil';
    } else if ((1000000 <= n) & (n < 1000000000)){
        base = 1000000.0;
        new_number = n*1.0/base;
        delta = 10000.0;
        mesh = 100.0;
        suffix = 'mill';
    } else {
        base = 1000000000.0;
        new_number = n*1.0/base;
        delta = 10000.0;
        mesh = 100.0;
        suffix = 'bill';
    }
    decimals = Math.round(Math.log10(base*1.0/(delta*1.0/mesh)));
    var result = sign * round_decimals(new_number, decimals);
    return result + ' ' + suffix;
}


StreamGraph.prototype.build_graphic = function (width_, heigth_){
    if (typeof width_ == "undefined"){
        width_ = 400;
    }
    if (typeof height_ == "undefined"){
        height_ = 300;
    }
    this.plt = new graph_function( {"height": height_,"width": width_,
                                    "container": "#" + this.container_id,
                                    "domain": [this.time - this.delta_time + this.delta_time/10.0,
                                               this.time + this.delta_time/10.0],
                                    "range": this.range,
                                    "x": {"format": secs2time},
                                    "y": {"format": round_custom}});
    this.plt.desactive_zoom_and_translation();
}


StreamGraph.prototype.add_curve = function(curve, color, auto_update){
    if (typeof auto_update == "undefined"){
        auto_update = true;
    }
    this.graphics[curve] = {"pp": undefined, "c": color, "u": auto_update};
}

StreamGraph.prototype.destroy = function (){
    d3.select("#" + this.container_id.replace(" ", "\\ ")).remove();
}


StreamGraph.prototype.update_top = function(time, price){
    this.range = [this.range[0], price];
    this.single_update(time);
}


StreamGraph.prototype.update_down = function(time, price){
    this.range = [price, this.range[1]];
    this.single_update(time);
}

StreamGraph.prototype.update_both = function(time, bottom_price, top_price){
    this.range = [bottom_price, top_price];
    this.single_update(time);
}


StreamGraph.prototype.__update_both = function(time, bottom_price, top_price){
    time = Math.max(time, this.time);
    var mid_price = (top_price + bottom_price)/2;
    var ratio = mid_price/(top_price - bottom_price);
    var d = 100000.0;
    var c = 5/d;
    if (ratio < 100000){
        this.range = [bottom_price, top_price];
    } else {
        var delta = c*mid_price/5;
        this.range = [mid_price-delta, mid_price+delta];
    }
//    this.range = [bottom_price, top_price];
    this.update_margin(time);
}


StreamGraph.prototype.add_object = function(objcts){
    var time = this.time;
    var obj;
    objcts.time = time;
    this.objects.splice(0, 0, objcts);
    this.__objects_count += objcts.length;
    if (this.auto_update_range){
        this.prices_list = this.prices_list.concat(objcts.range);
    }
    for (var i=this.objects.length-1; i >= 0 ; i--){
        obj = this.objects[i];
        if (obj.time + 100 <= time){
            this.prices_list.splice(0, obj.range.length);
            this.objects.splice(i, 1);
            this.plt.remove(obj);
        } else {
            break;
        }
    }
    this.update_range_graphic_limits();
}


StreamGraph.prototype.update_range_graphic_limits = function(){
    var l_price = Math.min.apply(Math, this.prices_list);
    var r_price = Math.max.apply(Math, this.prices_list);

    if ((isFinite(l_price)) && (isFinite(r_price))){
        var lag = (r_price - l_price + 1.0)/5.0;
        this.__update_both(this.time, l_price - lag, r_price + lag);
    }
}


StreamGraph.prototype.update_curve = function(curve, time, price){
    time = Math.max(time, this.time);
    if (this.__objects_count > this.__maximum_objects_number){
        this.__last_curves[curve] = price;
        return;
    }
    var c = this.graphics[curve];
    if (typeof c.pp != "undefined"){
        this.update_line(time, c.pp, price, c.c);
    }
    c.pp = price;
    this.single_update_curves(curve, time);
    this.update_margin(time);
}

StreamGraph.prototype.update_multiple_curves = function(curves, time){
    time = Math.max(time, this.time);
    var price;
    for (var curve in this.graphics){
        var c = this.graphics[curve];
        price = curves[curve];
        if (typeof price != "undefined"){
            if (typeof c.pp != "undefined"){
                this.update_line(time, c.pp, price, c.c);
            }
        }
        c.pp = price;
    }
    this.update_margin(time);
}

StreamGraph.prototype.single_update_curve = function(curve, time){
    time = Math.max(time, this.time);
    var c = this.graphics[curve];
    this.update_line(time, c.pp, c.pp, c.c);
}

StreamGraph.prototype.single_update_curves = function(curve, time){
    time = Math.max(time, this.time);
    var c;
    for (var g in this.graphics){
        if (g != curve){
            c = this.graphics[g];
            if (c.u == true){
                if (typeof c.pp != "undefined"){
                    this.update_line(time, c.pp, c.pp, c.c);
                }
            }
        }
    }
}


StreamGraph.prototype.single_update = function (time){
    time = Math.max(time, this.time);
    this.__last_time = time;
    if (this.__last_time - this.time > this.__refresh_time){
        this.__reset_count();
    }
}


StreamGraph.prototype.__single_update = function (time){
    var c;
    for (var g in this.graphics){
        c = this.graphics[g];
        if (typeof c.pp != "undefined"){
            this.update_line(time, c.pp, c.pp, c.c);
        }
    }
    this.update_margin(time);
}

StreamGraph.prototype.update_line = function(new_time, prev_price, price, color){
    try{
        if (this.mode == 0){
            var obj1 = this.plt.plot([this.time, new_time],
                                     [prev_price, prev_price],
                                     {"color": color});

            var obj2 = this.plt.plot([new_time, new_time],
                                     [prev_price, price],
                                     {"color": color});

            obj1.range = [prev_price];
            obj2.range = [prev_price, price];
            this.add_object(obj1);
            this.add_object(obj2);
        } else if (this.mode == 1){
            var obj1 = this.plt.plot([this.time, new_time],
                                     [prev_price, price],
                                     {"color": color});
            obj1.range = [prev_price];
            this.add_object(obj1);
         }
    } catch(e) {
        console.log("Se produjo el error ", e.message)
    }
}


StreamGraph.prototype.update_time = function (time){
    this.time = time;
}


StreamGraph.prototype.update_margin = function (time){
    this.plt.set_new_square([time - this.delta_time + this.delta_time/10.0,
                             time + this.delta_time/10.0],
                            this.range);
    if (this.auto_update_time == true){
        this.update_time(time);
    }
}