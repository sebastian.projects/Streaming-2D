
var plt = new graph_function({"height": 500,"width": 700, "container": "#here1", "domain": [-350, 350], "range": [-110, 110]});
var plt2 = new graph_function({"height": 500,"width": 700, "container": "#here2", "domain": [-350, 350], "range": [-110, 110]});

pas = {3: 45, 4: 0};
plt.poligon([[[-70, 0], [-80, -40], [30, -30], [4,6]], [[-70, -15], [-70, 15], [-40, 0]], [[70, -15], [70, 15], [40, 0]]], {"color": "red", "transform": function(d){return __rotate(pas[d.length]);}, "zoom": false});
plt.scatter([100, -100], [100, 100], 14, {"color": "green", "zoom": false});

plt.symbol([100, 100], [100, 50], {"zoom": false, "move": false, "symbol": function(d){var cas; if (d.d[1]<100) {cas = "triangle-up"} return cas}});

plt.rectangle([100], [-100], 10, 20, {"color": "red", "rotate": 35});
plt.dvector_field([[0, 100], [-100, 0], [0, -100], [100, 0]], [[-100, 0], [0, -100], [100, 0], [0, 100]], {"head-size": 4, "color": "green", "length": 1});

var xdata = [];
var ydata = [];

var i;

for (i=-314.15; i<=314.15; i+=1){
    xdata.push(i);
}

for (i in xdata){
    ydata.push(100*Math.sin(xdata[i]/100));
}

var eq2 = "f(x) = \\sqrt{a^2 + b^2}";

plt.add_to_legend({"symbol": "circle", "color": "green", "legend": "scatter green"});
plt.add_to_legend({"symbol": "square", "color": "red", "legend": "poligon"});
plt.add_to_legend({"symbol": "triangle-up", "color": "red", "legend": "triangle", "size": .8});
plt.add_to_legend({"symbol": "curve", "color": "red", "legend": "|$" + eq2 + "$|"});


plt2.plot(xdata, ydata, {"alpha": .5,"color": "red"});
//plt.Six_Paths_of_Pain2([{"d": [[-70, 0], [-80, -40], [30, -30], [4,6]], "c": [0, 0], "a": {"t":""}}, {"d": [[-70, -15], [-70, 15], [-40, 0]], "c": [0, 0], "a": {"t":""}}, {"d": [[70, -15], [70, 15], [40, 0]], "c": [0, 0], "a": {"t":""}}]);
