
var plt = new graph_function({"height": 500,"width": 700, "container": "#here1", "domain": [-50, 50], "range": [-50, 50]});

var data = [];
var i, j;

for (i=-50; i<=50; i+=5){
    for (j=-50; j<=50; j+=5){
        data.push([i, j]);
    }
}

plt.cvector_field(data, function(d){return [-d[1], d[0]];}, {"head-size": 2, "length": .4, "linewidth":.5, "color": "red", "linecolor": "red", "legend": "Campo|$ (-y, x) $|"});