//https://github.com/mbostock/d3/wiki/Time-Formatting
var plt = new graph_function({"height": 500,"width": 700, "container": "#here1", "range": [-110, 110], "x": {"type": "time"}});

plt.labels({"x": {"type": "time"}, "y": {"format": function(d){
                                    return (d%60) + "m";
                                }
                            }
})
var xdata = [];
var ydata = [];
var zdata = [];

xdata = [new Date(2015, 03, 20, 0, 0, 0, 0), new Date(2015, 03, 20, 18, 0, 0, 0), new Date(2015, 03, 20, 24, 0, 0, 0)];
ydata = [10, 50, 10];

plt.plot(xdata, ydata, {"alpha": .5, "legend": "Interpolado step-before", "color": "red", "interpolate": "step-before"});
plt.plot(xdata, ydata, {"alpha": .5, "legend": "Interpolado step-after", "color": "green", "interpolate": "step-after"});
plt.plot(xdata, ydata, {"alpha": .5, "legend": "Interpolado basis", "color": "blue", "interpolate": "basis"});


plt.scatter(xdata, ydata, 5, {"alpha": 1, "color": "black"});