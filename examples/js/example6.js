
var plt = new graph_function({"height": 500,"width": 700, "container": "#here1", "domain": [-1, 10], "range": [-1, 10]});
var data = [];
var i, j;

for (i=-1; i<= 10; i+=.1){
    for (j=-1; j<= 10; j+=.1){
        data.push([i, j]);
    }
}

plt.cvector_field(data, function(d){return [Math.sin(d[1]), Math.sin(d[0])];}, {"head-size": 1, "length": .1, "color": "green", "linecolor": "green"});