

var plt = new graph_function({"height": 500,"width": 700, "container": "#here1", "domain": [-350, 350], "range": [-110, 110]});

plt.rectangle([-100, 0], [-100, 0], 30, 20, {"color": "blue", "rotate": 30, "legend": "Rectangulos no escalables"});

plt.scatter([100], [100], 14, {"color": "green", "legend": "Circulo no escalable"});
plt.scatter([-100, 0], [-100, 0], 30, {"color": "red", "legend": "Circulos no escalables"});
plt.scatter([-50], [10], 14, {"color": "orange", "zoom": true, "legend": "Circulo escalable"});
plt.scatter([100], [100], 14, {"move": false, "color": "purple", "legend": "Circulo no modificable"});