
var LATEX_INITIAL_CHAR = "|$";
var LATEX_FINAL_CHAR = "$|";

function __render_tex(text, container, args){
    var id = "ID" + Math.floor(100000*Math.random());
    var pos1 = text.indexOf(LATEX_INITIAL_CHAR);
    var font_size;

    if (typeof args == "undefined"){
        args = {};
    }

    if ("font-size" in args){
        font_size = args["font-size"];
    } else {
        font_size = "12px";
    }


    if (text.trim().length == 0){
        return ;
    }

    if (pos1 < 0){
        d3.select(container).append("div").attr("class", "latex_class_text")
                                          .text(text)
                                          .style("font-size", font_size);
        return ;
    }

    var pos2 = text.substring(pos1, text.length).indexOf(LATEX_FINAL_CHAR)+pos1;
    if (pos2 < 0){
        d3.select(container).append("div").attr("class", "latex_class_text")
                                          .text(text)
                                          .style("font-size", font_size);
        return ;
    }

    var text1 = text.substring(0, pos1);
    var text2 = text.substring(pos2+LATEX_FINAL_CHAR.length, text.length);
    var to_render = text.substring(pos1+LATEX_INITIAL_CHAR.length, pos2);

    if (text1.trim().length > 0){
        d3.select(container).append("div").attr("class", "latex_class_text")
                                          .text(text1)
                                          .style("font-size", font_size);
    }
    d3.select(container).append("div").attr("class", "latex_class").attr("id", id)
                                                                    .attr("font-size", 2);
    katex.render(to_render, document.getElementById(id));

    if (text2.length > 0){
        __render_tex(text2, container);
    }
}