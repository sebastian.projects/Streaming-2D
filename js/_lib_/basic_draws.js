
//Convierte arreglos de la forma [a1, a2, ...], [b1, b2, ...] en [[a1, b1], [a2, b2], ...]
function __transform_data (datax, datay){
    var size = Math.min(datax.length, datay.length);
    var data = d3.range(size).map(function(i) {
      return [
        datax[i],
        datay[i]
      ];
    });
    return data;
}

graph_function.prototype.__apply_style = function (draw, args){
    if (typeof args !== "undefined"){
        if ("color" in args){
            draw.style('fill', function(d) { return args["color"]; });
        }

        if ("linewidth" in args){
            draw.style('stroke-width', function(d) { return args["linewidth"]; });
        }

        if ("linecolor" in args){
            draw.style('stroke', function(d) { return args["linecolor"]; });
        }

        if ("alpha" in args){
            draw.style("opacity", args["opacity"]);
        }
    }
}

graph_function.prototype.__apply_line_style = function(draw, args){
    if (typeof args !== "undefined"){
        if ("color" in args){
            draw.style('stroke', function(d) { return args["color"]; });
        }

        if ("linewidth" in args){
            draw.style('stroke-width', function(d) { return args["linewidth"]; });
        }

        if ("alpha" in args){
            draw.style("stroke-opacity", args["opacity"]);
        }
    }
}

graph_function.prototype.Six_Paths_of_Pain = function (points, args){
    var transformations;
    var box = this;
    var center;
    var zoom;
    var d;
    var i;
    var rotate;
    var conformal;
    var angle;

    var objects = [];
//  Formato de objects: [object1, object2, ...]
//  objecti = {"d": [puntos], "c": [centerx(), centery()], "s": [escalax, escalay], "t": transformations()}

    if (typeof args == "undefined"){
        args = {};
    }

    if ("center" in args){
        center = args["center"];
    }else{
        center = function(d){return [0,0];};
    }

    if ("rotate" in args){
        rotate = __rotate(args["rotate"]);
    }else{
        rotate = "";
    }

    if ("transform" in args){
        transform = args["transform"];
    }else{
        transform = function(d){return rotate;};
    }

    if ("zoom" in args){
        zoom = args["zoom"];
    }else{
        zoom = false;
    }

    if ("x_scale" in args){
        x_scale = args["x_scale"];
    }else{
        x_scale = false;
    }

    if ("y_scale" in args){
        y_scale = args["y_scale"];
    }else{
        y_scale = false;
    }

    if ("conformal" in args){
        conformal = args["conformal"];
    }else{
        conformal = false;
    }

    if ("angle" in args){
        angle = args["angle"];
    } else {
        angle = function(d){return 0;};
    }

    if (zoom | x_scale | y_scale){
        for (i in points){
            d = points [i];
            objects.push({"d": d, "c": center(d), "s": [this.__escalax, this.__escalay], "t": transform(d)});
        }
    } else if (conformal){
        for (i in points){
            d = points [i];
            objects.push({"d": d, "c": center(d), "a": convert_angle(angle(d)),"t": transform(d)});
        }
     } else {
        for (i in points){
            d = points [i];
            objects.push({"d": d, "c": center(d), "t": transform(d)});
        }
    }
    return objects;
}

graph_function.prototype.add_properties = function(args, draw){
    var transformation;
    var box = this;
    var zoom;
    var x_scale;
    var y_scale;
    var move;
    var _class;
    var __class;
    var draw;
    var conformal;

    if ("zoom" in args){
        zoom = args["zoom"];
    }else{
        zoom = false;
    }

    if (typeof draw == "undefined"){
        draw = this.svg;
    }

    if ("move" in args){
        move = args["move"];
    }else{
        move = true;
    }

    if ("x_scale" in args){
        x_scale = args["x_scale"];
    }else{
        x_scale = false;
    }

    if ("y_scale" in args){
        y_scale = args["y_scale"];
    }else{
        y_scale = false;
    }

    if ("conformal" in args){
        conformal = args["conformal"];
    }else{
        conformal = false;
    }

    if ("class" in args){
        _class = args["class"];
    }else{
        _class = function(d){return "__default_fig ";};
    }

    if (zoom){
        transformation = function(d){
            return __translate(box.__x_scale(d.c[0]), box.__y_scale(d.c[1])) + __scale(box.__escalax/d.s[0], box.__escalay/d.s[1]) + d.t;
        }
        __class = this.__CLASS_DEFAULT_ZOOMABLE;

    } else if (conformal){
        transformation = function(d){
            var lambda = (box.__x1 - box.__x0)/(box.__y0 - box.__y1);
            return __translate(box.__x_scale(d.c[0]), box.__y_scale(d.c[1])) + __rotate(real_angle(lambda, +d.a)) + d.t;
        }
        __class = this.__CLASS_DEFAULT_CONFORMAL;

    } else if (x_scale) {
        transformation = function(d){
            return __translate(box.__x_scale(d.c[0]), box.__y_scale(d.c[1])) + __scale(box.__escalax/d.s[0], box.__escalay/d.s[1]) + d.t;
        }
        __class = this.__CLASS_DEFAULT_X_SCALABLE;

    } else if (y_scale) {
        transformation = function(d){
            return __translate(box.__x_scale(d.c[0]), box.__y_scale(d.c[1])) + __scale(box.__escalax/d.s[0], box.__escalay/d.s[1]) + d.t;
        }
        __class = this.__CLASS_DEFAULT_Y_SCALABLE;

    } else if (move) {
        transformation = function(d){
            return __translate(box.__x_scale(d.c[0]), box.__y_scale(d.c[1])) + d.t;
        }
        __class = this.__CLASS_DEFAULT_MOVABLE;

    } else {
        transformation = function(d){
            return __translate(d.c[0], d.c[1]) + d.t;
        }
        __class = this.__CLASS_DEFAULT_TRANSFORMABLE;
    }

    draw.attr("class", function(d){return _class(d) + " " + __class});
    draw.attr("transform", transformation);
    return draw;
}

graph_function.prototype.autolegend = function(args, default_symbol){
    var symbol;
    var legend;

    if (!("legend" in args)){
        return ;
    }

    if (!("symbol" in args)){
        args["symbol"] = default_symbol;
    }
    this.add_to_legend(args);
}

graph_function.prototype.poligon = function (objects, args){
    var box = this;
    var data = this.Six_Paths_of_Pain(objects, args);
    var zoom;
    var interpolate;
    var path_g;

    if ("interpolate" in args){
        interpolate = args["interpolate"];
    }else{
        interpolate = "linear";
    }

    if ("zoom" in args){
        zoom = args["zoom"];
    }else{
        zoom = false;
    }

    if (zoom){
        this.CLASS_BASIC_LINE.interpolate(interpolate);
        path_g = this.svg.selectAll("paths")
                    .data(data)
                .enter().append("g");
        path_g.attr("clip-path", "url(#"+this.__clip_id+")")
        var path = path_g.append("path")
                .attr("d", function(d){return box.CLASS_BASIC_LINE(d.d);});
    } else {
        this.CLASS_BASIC_LINE_PIXEL.interpolate(interpolate);
        path_g = this.svg.selectAll("paths")
                    .data(data)
                .enter().append("g");

        path_g.attr("clip-path", "url(#"+this.__clip_id+")")
        var path = path_g.append("path")
                    .attr("d", function(d){return box.CLASS_BASIC_LINE_PIXEL(d.d);});
    }

    this.add_properties(args, path);
    this.__apply_style(path, args);
    this.autolegend(args, "square");
    return path_g;
}

graph_function.prototype.scatter = function (xdata, ydata, radius, args){

    if (typeof args == "undefined"){
        args = {};
    }

    if (!("center" in args)){
        args["center"] = function(d){return d;};
    }
    var data = this.Six_Paths_of_Pain(__transform_data(xdata, ydata), args);
    var box = this;

    var path_g = this.svg.selectAll("circles")
        .data(data)
      .enter().append("g");

      path_g.attr("clip-path", "url(#"+this.__clip_id+")")
      var path = path_g.append("circle")
        .attr("r", radius);

    this.add_properties(args, path);
    this.__apply_style(path, args);
    this.autolegend(args, "circle");
    return path_g;
}

graph_function.prototype.text = function (data, text, args){

    if (typeof args == "undefined"){
        args = {};
    }

    if (!("center" in args)){
        args["center"] = function(d){return d;};
    }
    var data = this.Six_Paths_of_Pain(__transform_data(xdata, ydata), args);
    var box = this;

    var path_g = this.svg.selectAll("circles")
        .data(data)
      .enter().append("g");

    path_g.attr("clip-path", "url(#"+this.__clip_id+")")

    var path = path_g.append("text")
        .text(function(d){return text(d);});

    this.add_properties(args, path);
    this.__apply_style(path, args);
    this.autolegend(args, "circle");
    return path_g;
}

graph_function.prototype.symbol = function(xdata, ydata, args){
    var symbol;
    var size;

    if (typeof args == "undefined"){
        args = {};
    }

    if ("symbol" in args){
        symbol = args["symbol"];
    }else{
        symbol = "circle";
    }

    if ("size" in args){
        size = args["size"];
    }else{
        size = 80;
    }

    if (!("center" in args)){
        args["center"] = function(d){return d;};
    }

    var data = this.Six_Paths_of_Pain(__transform_data(xdata, ydata), args);

    var path_g = this.svg.selectAll("paths")
        .data(data)
      .enter().append("g");

    path_g.attr("clip-path", "url(#"+this.__clip_id+")")
    var path = path_g.append("path")
        .attr("d", function(d){
                        return d3.svg.symbol().type( symbol )
                                              .size( size ) (d);
                   }
        );

    this.add_properties(args, path);
    this.__apply_style(path, args);
    this.autolegend(args, "diamond");
    return path_g;
}

graph_function.prototype.rectangle = function (xdata, ydata, width, height, args){
    if (!("center" in args)){
        args["center"] = function(d){return d;};
    }
    var data = this.Six_Paths_of_Pain(__transform_data(xdata, ydata), args);

    var path_g = this.svg.selectAll("rects")
        .data(data)
      .enter().append("g");

    var x_scale = false;
    var y_scale = false;
    var real_height;
    var real_width;

    if ("x_scale" in args){
        x_scale = args["x_scale"];
    }else{
        x_scale = false;
    }

    if ("y_scale" in args){
        y_scale = args["y_scale"];
    }else{
        y_scale = false;
    }

    if (x_scale){
        real_width = Math.abs(this.__x_scale(width) - this.__x_scale(0));
    } else {
        real_width = width;
    }

    if (y_scale){
        real_height = Math.abs(this.__y_scale(height) - this.__y_scale(0));
    } else {
        real_height = height;
    }

    path_g.attr("clip-path", "url(#"+this.__clip_id+")")
    var path = path_g.append("rect")
        .attr("width", real_width)
        .attr("height", real_height);

    this.add_properties(args, path);
    this.__apply_style(path, args);
    this.autolegend(args, "square");
    return path_g;
}

graph_function.prototype.plot = function (xdata, ydata, args){
    if (typeof args == "undefined"){
        args = {};
    }

    var interpolate;
    if ("interpolate" in args){
        interpolate = args["interpolate"];
    }else{
        interpolate = "linear";
    }
    this.CLASS_BASIC_LINE.interpolate(interpolate);

    if (!("zoom" in args)){
        args["zoom"] = true;
    }

    var data = this.Six_Paths_of_Pain([__transform_data(xdata, ydata)], args);
    var _class;
    var box = this;

    if (!("class" in args)){
        args["class"] = function(d){return "__default_line ";};
    }
    var path_g = this.svg.selectAll("paths")
                .data(data)
            .enter().append("g");

    path_g.attr("clip-path", "url(#"+this.__clip_id+")");
    var path = path_g.append("path")
        .attr("d", function(d){return box.CLASS_BASIC_LINE(d.d);});

    this.add_properties(args, path);
    this.__apply_line_style(path, args);
    this.autolegend(args, "curve");
    return path_g;
}

graph_function.prototype.arrow = function(domain, length, slope, args){

//ESTA FORMA DE DIBUJAR FLECHAS, INDEPENDE LA MAGNITUD DEL VECTOR CON EL ZOOM
//LA MAGNITUD DE LOS VECTORES SE HACE ENTONCES DE FORMA INFORMATIVA PARA
//MOSTRAR LA INTENSIDAD DE CADA PUNTO RESPECTO A OTRO. SIN EMBARGO, SI SE DESEA
//CONSERVAR LA MAGNITUD EN TODO MOMENTO, ES MAS FACIL DIBUJARLO.
    var box = this;
    var head_size;
    var zoom;



    if (typeof args == "undefined"){
        args = {};
    }
    var head_size;

    if ("head-size" in args){
        head_size = args["head-size"];
    }else{
        head_size = 2.5;
    }

    if ("zoom" in args){
        zoom = args["zoom"];
    }else{
        zoom = true;
    }

    if ("conformal" in args){
        conformal = args["conformal"];
    }else{
        conformal = false;
    }

    var box = this;
    var angle = function(x){return Math.degrees(Math.atan(slope(x)/box.__lambda));}

    if (conformal){
        args["angle"] = function(d){
                                return  -angle(d);
                            };
    } else {
        args["transform"] = function(d){
                                return  __rotate(-angle(d));
                            };
    }


    var interpolate;
    if ("interpolate" in args){
        interpolate = args["interpolate"];
    }else{
        interpolate = "linear";
    }
    this.CLASS_BASIC_LINE_PIXEL.interpolate(interpolate);

    var trangle = [-1 * head_size, 1 * head_size, 1.8 * head_size];
    var head = function(d){
                    return [[length(d), trangle[0]], [length(d), trangle[1]], [length(d) + trangle[2], 0]];
                };
    var tail = function(d){
                    return [[0, 0], [length(d), 0]];
                };

    args["center"] = function(d){
                        return d;
                     };

    var data = this.Six_Paths_of_Pain(domain, args);

    var path_head_g = this.svg.selectAll("paths")
                .data(data)
            .enter().append("g");
    path_head_g.attr("clip-path", "url(#"+this.__clip_id+")")
    var path_head = path_head_g.append("path")
                .attr("d", function(d){
                                if (Math.abs(length(d.d)) < 10e-10){
                                    return d3.svg.symbol().type( "circle" )
                                                          .size( 10*head_size ) (d.d);
                                } else {
                                    return box.CLASS_BASIC_LINE_PIXEL(head(d.d));
                                }
                           }
                );

    var path_tail_g = this.svg.selectAll("paths")
                .data(data)
            .enter().append("g");
    path_tail_g.attr("clip-path", "url(#"+this.__clip_id+")")
    var path_tail = path_tail_g.append("path")
                .attr("d", function(d){return box.CLASS_BASIC_LINE_PIXEL(tail(d.d));});


    this.add_properties(args, path_head);

    args["class"] = function(d){return"__default_line ";};
    this.add_properties(args, path_tail);

    this.__apply_style(path_head, args);
    this.__apply_style(path_tail, args);

    this.autolegend(args, "triangle-up");
    var res = [path_tail_g, path_head_g];
    res.remove = function(){
        path_tail_g.remove();
        path_head_g.remove();
    }
    return res;
}

graph_function.prototype.cvector_field = function(domain, f, args){
    var box = this;
    var length;
    if (typeof args == "undefined"){
        args = {};
    }

    if ("length" in args){
        length = args["length"];
    }else{
        length = 1;
    }

    var f_angle = function(d){
        var fun = f(d);
        var fx = fun[0];
        var fy = fun[1];
        var angle;

        if (fx == 0){
            if (fy == 0){
                return 0;
            } else if (fy < 0){
                angle = -90;
            } else {
                angle = 90;
            }
        } else {
            angle = Math.atan(fy/fx)*180/Math.PI;
            if (fx < 0){
                angle = 180 + angle;
            }
        }
        return angle;
    }

    var f_length = function(d){
        var fun = f(d);
        return length*Math.sqrt(Math.pow(fun[0], 2.0) + Math.pow(fun[1], 2.0));
    }

    return this.arrow(domain, f_length, f_angle, args);
}

graph_function.prototype.dvector_field = function(domain, f, args){
    var _f = {};
    for (i=0; i<domain.length; i++){
        _f[domain[i]] = f[i];
    }

    var f = function(d){
        return _f[d];
    }

    return this.cvector_field(domain, f, args)
}

graph_function.prototype.remove = function (obj){
    obj.remove();
}

function real_angle(lambda, angle){
    var rangle;
    rangle = 180*Math.atan(Math.tan(angle*Math.PI/180)*lambda)/Math.PI;
    return rangle + quadrant(angle);
}

function quadrant(angle){
    if ((0 <= angle) & (angle <= 90)){
        return 0;
    }
    if ((90 < angle) & (angle <= 180)){
        return 180;
    }
    if ((-90 <= angle) & (angle < 0)){
        return 0;
    }
    if ((-180 < angle) & (angle < -90)){
        return 180;
    }
}

// Converts from degrees to radians.
Math.radians = function(degrees) {
  return degrees * Math.PI / 180;
};

// Converts from radians to degrees.
Math.degrees = function(radians) {
  return radians * 180 / Math.PI;
};

function convert_angle(angle){
    return angle + Math.floor((180-angle)/360)*360;
}