
graph_function.prototype.jump = function(xmovement, ymovement, duration){
  var box = this;
  this.svg.transition()
      .duration(duration)
      .call(box.__zoom.translate([xmovement, ymovement]).event);
}

graph_function.prototype.set_new_square = function(domain, range){
    this.initial_domain = domain;
    this.initial_range = range;

    this.__x_scale.domain([domain[0], domain[1]]);
    this.__y_scale.domain([range[0], range[1]]);
    this.__set_scales();
    this.svg.call(this.__zoom
        .x(this.__x_scale)
        .y(this.__y_scale)
        .event);
}

function moveme(){
//plt.set_new_square([0, 200], [-200, 100])
plt.jump(100, 100, 5000)
}
