
var LEGEND_LEFT_UP = "LEGEND_LEFT_UP";
var LEGEND_LEFT_DOWN = "LEGEND_LEFT_DOWN";
var LEGEND_RIGHT_UP = "LEGEND_RIGHT_UP";
var LEGEND_RIGHT_DOWN = "LEGEND_RIGHT_DOWN";
var LEGEND_CENTER = "LEGEND_CENTER";


var graph_function = function(args){
    var height;
    var width;
    var _container;
    var phisic_container;

    //#############################################################################
    //  Declaraciones iniciales.
    //  TENER CUIDADO AL RENOMBRARLAS, YA QUE ALGUNAS ESTAN EN OTROS MODULOS.
    //  Si se va a renombrar alguna variable, checkear primero si se utiliza en
    //  basic_draws, class_zoom, control_plane (u otros modulos adjuntos)
    //#############################################################################

    //Variables globales

    if ("container" in args){
        _container = args["container"];
    } else {
        _container = "body";
    }

    var container = _container + "_hq";
    var phisic_container = d3.select(_container.replace(" ", "\\ ")).append("div").attr("id", container.substring(1, container.length))
                                    .style("height", args["height"] + "px")
                                    .style("width", args["width"] + "px").node();

//    phisic_container = d3.select(container).node();
    if ("height" in args){
        height = args["height"];
    } else {
        height = phisic_container.offsetHeight;
    }
    if ("width" in args){
        width = args["width"];
    } else {
        width = phisic_container.offsetWidth;
    }

    //Cada objeto que herede de esta clase, (y que se haya añadido al plano)
    //sera movido automaticamente con los eventos de mouse. NO se hace zoom.
    this.__CLASS_DEFAULT_MOVABLE = "__default_movable";

    //Aqui se pueden agregar todos los objetos que se deseen zoomear con el evento scroll por
    //y que se agreguen a la escena una vez que se dibuje el plano.
    //Si se agregan despues de dibujado el plano, es mejor utilizar el otro metodo.
    this.__CLASS_DEFAULT_ZOOMABLE = "__default_zoomable";
    this.__CLASS_DEFAULT_X_SCALABLE = "__default_x_scale";
    this.__CLASS_DEFAULT_Y_SCALABLE = "__default_y_scale";
    this.__CLASS_DEFAULT_TRANSFORMABLE = "__default_transformable";
    this.__CLASS_DEFAULT_CONFORMAL = "__default_conformal";


    //Dimensiones del plano
    this.margin = {top: 20, right: 20, bottom: 30, left: 55};
    this.__box_width = width - this.margin.left - this.margin.right;
    this.__box_height = height - this.margin.top - this.margin.bottom;

    //Definicion de las posiciones de la leyenda
    this.__LEGEND_PLACES = {LEGEND_LEFT_UP:    [this.margin.left*1.2, this.margin.top*1.4],
                            LEGEND_LEFT_DOWN:  [this.margin.left, this.__box_height*.8 + this.margin.top],
                            LEGEND_RIGHT_DOWN: [this.__box_width*.8 + this.margin.left, this.__box_height*.8 + this.margin.top],
                            LEGEND_RIGHT_UP:   [this.__box_width*.8 + this.margin.left, this.margin.top*1.4],
                            LEGEND_CENTER:     [this.__box_width*.5 + this.margin.left, this.__box_height*.5 + this.margin.top]
                            };

    this.__LEGEND_WIDTH = this.__box_width*.2;
    this.__LEGEND_ITEM_HEIGHT = 7;
    this.__LEGEND_HEIGHT = 8;

    //Dibujar plano
    this.__set_plane(args)
    this.__clip_id = "clip" + (Math.round(Math.random()*1000));
    this.__container = container.replace(" ", "\\ ");
    this.__draw_plane();

    //Para dibujar curvas.
    var __x_scale = this.__x_scale;
    var __y_scale = this.__y_scale;
    this.CLASS_BASIC_LINE = d3.svg.line()
        .x(function(d) { return __x_scale(d[0])-__x_scale(0); })
        .y(function(d) { return __y_scale(d[1])-__y_scale(0); });

    this.CLASS_BASIC_LINE_PIXEL = d3.svg.line()
        .x(function(d) { return d[0]; })
        .y(function(d) { return d[1]; });

    //#############################################################################
    //  Clases para los efectos.
    //#############################################################################

    //Definicion de clases para el auto-zoom y el movimiento automatico cuando
    //Se interactue con el mouse. Si un objeto es añadido a esta clase, cada
    //vez que por ejemplo se haga scroll, este objeto aumentara de tamaño.

    //#############################################################################
    //#############################################################################
    //#############################################################################
}

graph_function.prototype.__set_scales = function(){
    this.__x0 = this.__x_scale(this.__zero_x);
    this.__x1 = this.__x_scale(this.__one_x);
    this.__y0 = this.__y_scale(this.__zero_y);
    this.__y1 = this.__y_scale(this.__one_y);
    this.__escalax = this.__x1 - this.__x0;
    this.__escalay = this.__y1 - this.__y0;
    this.__lambda = (this.__x1 - this.__x0)/(this.__y0 - this.__y1);
}

graph_function.prototype.opaque = function(what){
    if (what){
        this.svg.style("opacity", 0.4);
        this.legend_container.style("opacity", 0.4);
    } else {
        this.svg.style("opacity", 1.0);
        this.legend_container.style("opacity", 0.6);
    }
}

graph_function.prototype.__draw_plane = function(){
    //#############################################################################
    //  Dibujando el plano
    //#############################################################################
    //Objeto necesario para controlar el zoom.
    this.__zoom = null;

    this.__fake_zoom = null;

    //Declaracion del area de dibujado
    this.svg = d3.select(this.__container).append("svg")
        .attr("class", "jsglsvg")
        .attr("width", this.__box_width + this.margin.left + this.margin.right)
        .attr("height", this.__box_height + this.margin.top + this.margin.bottom)
      .append("g")
        .attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")");

    this.svg.append("defs").append("clipPath").attr("id", this.__clip_id)
        .append("rect").attr("width", this.__box_width).attr("height", this.__box_height);

    this.svg.append("rect")
        .attr("width", this.__box_width)
        .attr("height", this.__box_height);

    this.__add_axis();
    this.__active_zoom_and_translation();
}

graph_function.prototype.labels = function (args){
    if (!("domain" in args)){
        args["domain"] = this.initial_domain;
    };
    if (!("range" in args)){
        args["range"] = this.initial_range;
    };

    this.__set_plane(args);
    this.set_new_square(args["domain"], args["range"]);
}

graph_function.prototype.__set_plane = function (args){
    var domain;
    var range;
    var infox;
    var infoy;

    if ("domain" in args){
        domain = args["domain"];
    }
    if ("range" in args){
        range = args["range"];
    }

    if ("x" in args){
        infox = this.__info_from_type(args["x"]);
    } else {
        infox = this.__info_from_type({});
    }

    if ("y" in args){
        infoy = this.__info_from_type(args["y"]);
    } else {
        infoy = this.__info_from_type({});
    }

    if (typeof domain == "undefined"){
        domain = infox["i"];
    }

    if (typeof range == "undefined"){
        range = infoy["i"];
    }

    //Definicion del plano inicial.
    this.initial_domain = domain;
    this.initial_range = range;

    this.__zero_x = infox[0];
    this.__zero_y = infoy[0];
    this.__one_x = infox[1];
    this.__one_y = infoy[1];
    //Estos son basicamente traductores. Cambian las coordenadas en pixeles
    //desde el punto superior izquierdo a coordenadas reales en el plano.
    //Es util cuando se desea posicionar un objeto en el plano sabiendo
    //sus coordenadas cartesianas.
    this.__x_scale = infox["s"];
    this.__y_scale = infoy["s"];

    this.__y_scale.domain([this.initial_range[0], this.initial_range[1]])
                  .range([this.__box_height, 0]);
    this.__x_scale.domain([this.initial_domain[0], this.initial_domain[1]])
                  .range([0, this.__box_width]);

    //Para dibujar los ejes, representan el eje x y el y respectivamente
    this.__xAxis = d3.svg.axis()
        .scale(this.__x_scale)
        .orient("bottom")
        .tickFormat(infox[2])
        .ticks(infox['t'])
        .tickSize(-this.__box_height);

    this.__yAxis = d3.svg.axis()
        .scale(this.__y_scale)
        .orient("left")
        .tickFormat(infoy[2])
        .ticks(infoy['t'])
        .tickSize(-this.__box_width);

    this.__set_scales();
}

graph_function.prototype.__info_from_type = function(args){
    var info;
    var scalar;
    var zero;
    var one;
    var tickFormat;
    var ticks = 4;

    switch (args["type"]){
        case "time":
            info = [new Date(2015, 03, 20), new Date(2015, 03, 21)];
            zero = new Date(2015, 03, 20);
            one = new Date(2015, 03, 21);
            scalar = d3.time.scale();
            //https://github.com/mbostock/d3/wiki/Time-Formatting
            tickFormat = d3.time.format("%I:%M:%S %p");
            break ;

        default :
            info = [-1, 1];
            zero = 0;
            one = 1;
            scalar = d3.scale.linear();
            tickFormat = function(d){return d;};
            break ;
    }
    if ("format" in args){
        tickFormat = args["format"];
    }
    if ("tick" in args){
        ticks = args["tick"];
    }
    return {"i": info, "s": scalar, 2: tickFormat, 0: zero, 1: one, 't': ticks};
}

//Dibuja los ejes.
graph_function.prototype.__add_axis = function(){
    this.svg.append("g")
        .attr("class", "__x __axis")
        .attr("transform", "translate(0," + this.__box_height + ")")
        .call(this.__xAxis);

    this.svg.append("g")
        .attr("class", "__y __axis")
        .call(this.__yAxis);


    var box = this;

//Cuando el usuario se mueve por el plano, se llama esta funcion.
//Esta a su vez se encarga de actualizar la posicion de los objetos segun el eje.
    this.__zoom = d3.behavior.zoom()
        .x(box.__x_scale)
        .y(box.__y_scale)
        .scaleExtent([-1, Infinity])
        .on("zoom", function(){
                box.__lambda = (box.__x1 - box.__x0)/(box.__y0 - box.__y1);
                box.__escalax = (box.__x_scale(box.__one_x) - box.__x_scale(box.__zero_x));
                box.__escalay = (box.__y_scale(box.__one_y) - box.__y_scale(box.__zero_y));

                box.svg.selectAll("." + box.__CLASS_DEFAULT_ZOOMABLE).attr("transform",
                                    function(d){
                                        return __translate(box.__x_scale(d.c[0]), box.__y_scale(d.c[1])) + __scale(box.__escalax/d.s[0], box.__escalay/d.s[1]) + d.t;
                                    }
                                );

                box.svg.selectAll("." + box.__CLASS_DEFAULT_X_SCALABLE).attr("transform",
                                    function(d){
                                        return __translate(box.__x_scale(d.c[0]), box.__y_scale(d.c[1])) + __scale(box.__escalax/d.s[0], 1.0) + d.t;
                                    }
                                );

                box.svg.selectAll("." + box.__CLASS_DEFAULT_Y_SCALABLE).attr("transform",
                                    function(d){
                                        return __translate(box.__x_scale(d.c[0]), box.__y_scale(d.c[1])) + __scale(1.0, box.__escalay/d.s[1]) + d.t;
                                    }
                                );

                box.svg.selectAll("." + box.__CLASS_DEFAULT_MOVABLE).attr("transform",
                                    function(d){
                                        return __translate(box.__x_scale(d.c[0]), box.__y_scale(d.c[1])) + d.t;
                                    }
                                );

                box.svg.selectAll("." + box.__CLASS_DEFAULT_TRANSFORMABLE).attr("transform",
                                    function(d){
                                        return __translate(d.c[0], d.c[1]) + d.t;
                                    }
                                );

                box.svg.selectAll("." + box.__CLASS_DEFAULT_CONFORMAL).attr("transform",
                                    function(d){
                                        return __translate(box.__x_scale(d.c[0]), box.__y_scale(d.c[1])) + __rotate(real_angle(box.__lambda, +d.a)) + d.t;
                                    }
                                );

                box.svg.select(".__x.__axis").call(box.__xAxis);
                box.svg.select(".__y.__axis").call(box.__yAxis);
        });

    this.__fake_zoom = d3.behavior.zoom()
}
//Funcion de zoom. Todos los elementos que se deseen, tengan la propiedad de
//zoom deben ser manualmente activados desde aqui.

//Activa el zoom y el desplazamiento.
graph_function.prototype.__active_zoom_and_translation = function(){
    var box = this;
    this.svg.call(box.__zoom);
}

//Si se desea desactivar el zoom.
graph_function.prototype.desactive_zoom_and_translation = function(){
    var box = this;
    this.svg.call(box.__fake_zoom);
}

function __rotate(angle){
    return " rotate(" + angle + ")" ;
}
function __translate(x, y){
    return " translate(" + x + "," + y + ")" ;
}
function __scale(x, y){
    return " scale(" + x + "," + y + ")" ;
}
function __matrix(M){
    return " matrix(" + M + ")" ;
}